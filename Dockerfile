FROM python:3.6

RUN mkdir -p /baka
WORKDIR /baka

COPY . /baka

RUN pip install flask
RUN pip install web.py

CMD ["python", "web.py"]

CMD python app.py


EXPOSE 8080
EXPOSE 5000
EXPOSE 3000
