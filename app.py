from flask import Flask, render_template

app = Flask(__name__)



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/shirts.html')
def shirts():
    return render_template('shirts.html')

@app.route('/figures.html')
def figures():
        return render_template('figures.html')

@app.route('/mark.html')
def mark():
        return render_template('mark.html')


@app.route('/money.html')
def money():
        return render_template('money.html')

@app.route('/about_us.html')
def about_us():
        return render_template('about_us.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0',port="8080",debug="True")
